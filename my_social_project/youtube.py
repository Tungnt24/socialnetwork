import youtube_dl
from flask import abort


def video_url(url):
    try:
        audio_dwlopt = {
            'format': 'bestaudio/best',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '128'
            }]
        }
        with youtube_dl.YoutubeDL(audio_dwlopt) as audio:
            audio.download([url])
    except Exception:
        abort(404)
