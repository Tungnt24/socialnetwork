from flask import Flask, render_template, request, redirect, url_for, send_file, abort
from insta import get_data
from youtube import video_url
from fb import get_url_video

app = Flask(__name__)


@app.route('/')
@app.route('/home')
def home():
    return render_template('home.html')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


app.register_error_handler(404, page_not_found)


@app.route('/socialnetwork')
def social():
    return render_template('SN.html')

@app.route("/images/<image_name>")
def return_image(image_name):
    try:
        image_path = f"images/{image_name}"
        return send_file(image_path, mimetype='image/gif')
    except FileNotFoundError as e:
        return abort(400)

@app.route('/dwlimg', methods=['GET', 'POST'])
def download_img():
    if request.method == "POST":
        insta_url = request.form['insta_url']
        image_name = get_data(insta_url)
        return redirect(f"images/{image_name}")
    else:
        return redirect(url_for('home'))


@app.route('/dwlmp3', methods=['GET', 'POST'])
def download_audio():
    if request.method == "POST":
        yt_url = request.form['yt_url']
        video_url(yt_url)
        return redirect(url_for('social'))
    else:
        return redirect(url_for('home'))


@app.route('/dwlmp4', methods=['GET', 'POST'])
def download_video():
    if request.method == 'POST':
        fb_url = request.form['fb_url']
        get_url_video(fb_url)
        return redirect(url_for('social'))
    else:
        return redirect(url_for('home'))


if __name__ == '__main__':
    app.run()
