import requests
import urllib.request
from bs4 import BeautifulSoup
import json
from random import randint
from flask import abort, send_file


def get_data(url):
    try:
        response = requests.get(str(url)).text
        html = BeautifulSoup(response, 'lxml')
        script = html.body.script.text
        json_data = script[21:len(script)-1]
        convert_dict = json.loads(json_data)
        media = convert_dict['entry_data']['PostPage'][0]['graphql']['shortcode_media']
        image_url = get_image_url(media)
        return image_url
    except Exception:
        return abort(404)


def get_image_url(media):
    if not media['is_video']:
        try:
            nodes = media['edge_sidecar_to_children']['edges']
            url_data = []
            for index in range(0, len(nodes)):
                url_img = nodes[index]['node']['display_url']
                url_data.append(url_img)
            for image in url_data:
                download_img(image)
        except KeyError:
            url_img = media['display_url']
            return download_img(url_img)
    else:
        return abort(404)


def download_img(url):
    img_name = f'{str(randint(1,1000))}.jpg'
    image_path = f"images/{img_name}"
    download = urllib.request.urlretrieve(url, image_path)
    return img_name
