import requests
import re
import urllib.request
from random import randint
from flask import abort


def get_url_video(url):
    try:
        html = requests.get(url).text
        if 'hd_src' in html:
            video_url = re.search('hd_src:"(.+?)"', html).group(1)
        else:
            video_url = re.search('sd_src:"(.+?)"', html).group(1)
        return download_video(video_url)
    except Exception:
        abort(404)


def download_video(url):
    name_video = f'{str(randint(1,1000))}.mp4'
    download = urllib.request.urlretrieve(url, name_video)
    return download
